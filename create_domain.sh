#!/bin/sh

. ./.env

CWD=$(pwd)

if [ $# != 1 ] ; then
    echo "Usage: $0 [DOMAIN]"
    exit 0
fi

export ILIAS_FOLDER=${ILIAS_FOLDER}

# write tpl.env file
envsubst '\
${ILIAS_FOLDER}\
' < .env > tpl.env

export TPL=$1

FOLDER="${TPL}-docker"

if [ -d "../${FOLDER}" ] ; then
    echo "../${FOLDER} already exists, please remove..."
    exit 0
fi 

# ilServer.ini 
export IP_ADDRESS=${LUCENE_IP_ADDRESS}
export PORT=${LUCENE_PORT}
export INDEX_PATH=${LUCENE_INDEX_PATH}
export LOG_FILE=${LUCENE_LOG_FILE}
export LOG_LEVEL=${LUCENE_LOG_LEVEL}
export NUM_THREADS=${LUCENE_NUM_THREADS}
export RAM_BUFFER_SIZE_THREADS=${LUCENE_RAM_BUFFER_SIZE_THREADS}
export INDEX_MAX_FILE_SIZE_MB=${LUCENE_INDEX_MAX_FILE_SIZE_MB}
export CLIENT_ID=$TPL
export NIC_ID=${LUCENE_NIC_ID}
export ILIAS_INI_PATH=${LUCENE_ILIAS_INI_PATH}

mkdir -p "../${FOLDER}"/config

envsubst '\
${TPL}\
' < tpl.env > "../${FOLDER}/.env"

envsubst '\
${TPL}\
' < tpl.docker-compose.yml > "../${FOLDER}/docker-compose.yml" 

envsubst '\
${TPL}\
' < tpl.php_enter.sh > "../${FOLDER}/php_enter.sh"

envsubst '\
${TPL}\
' < tpl.nginx_enter.sh > "../${FOLDER}/nginx_enter.sh"

envsubst '\
${TPL}\
' < tpl.java_enter.sh > "../${FOLDER}/java_enter.sh"

envsubst '\
${TPL}\
' < tpl.java_start.sh > "../${FOLDER}/java_start.sh"

envsubst '\
${TPL}\
' < tpl.java_stop.sh > "../${FOLDER}/java_stop.sh"

envsubst '\
${TPL}\
' < tpl.config.json > "../${FOLDER}/config.json"

envsubst '\
${IP_ADDRESS}\
${PORT}\
${INDEX_PATH}\
${LOG_FILE}\
${LOG_LEVEL}\
${NUM_THREADS}\
${RAM_BUFFER_SIZE_THREADS}\
${INDEX_MAX_FILE_SIZE_MB}\
${CLIENT_ID}\
${NIC_ID}\
${ILIAS_INI_PATH}\
' < tpl.ilServer.ini > "../${FOLDER}/config/ilServer.ini"

# copy all required files outside into ${FOLDER} Folder
for i in up.sh down.sh net.sh
do
  cp "$i" "../${FOLDER}/"
done

chmod 755 "../${FOLDER}/"*.sh

cd "../${FOLDER}"
./up.sh

docker exec -t il-php-${TPL}-cont /usr/local/bin/libs-install.sh

#if [ "${CLI_SETUP}" != "1" ] ; then
#    exit 0
#fi
#cp "config.json" "${ILIAS_FOLDER}/${TPL}/"
#docker exec -t il-php-${TPL}-cont /usr/local/bin/setup.sh

# due to an unknown bug in cli-setup:

#cd $CWD

#envsubst '\
#${TPL}\
#' < tpl.ilias.ini.php > "${ILIAS_FOLDER}/${TPL}/ilias.ini.php"

#envsubst '\
#${TPL}\
#' < tpl.client.ini.php > "${ILIAS_FOLDER}/${TPL}/data/${TPL}/client.ini.php"

#exit 0
