#!/usr/bin/env bash

. ./.env

cd ${ILIAS_FOLDER}

if [ $# != 3 ]; then
    echo "Please set 'repo branch folder' name for cloning"
    exit 1
fi

REPO=$1
BRANCH=$2
FOLDER=$3

if [ ! -d "${REPO}" ] ; then
    echo "${REPO} does not exist!"
    exit 1
fi

if [ -d "${BRANCH}"  ] ; then
    echo "${BRANCH} already exists! Please remove folder."
    exit 1
fi

cd $REPO
git checkout $BRANCH
git pull

cd ..

git clone -l $REPO --branch $BRANCH $FOLDER

ln -sf "$FOLDER" "${FOLDER}-web"

if [ -d "${FOLDER}-data" ] ; then
    echo "${FOLDER}-data already exists."
    exit 0
fi

mkdir "${FOLDER}-data"
