#!/bin/sh

. ./.env

CWD=$(pwd)

if [ $# != 1 ] ; then
    echo "Usage: $0 [DOMAIN]"
    exit 0
fi

DOMAIN=$1
REGISTRY="registry.gitlab.com/eqsoft/ilias-data:${DOMAIN}"

# ensure docker container are down
cd ../${DOMAIN}-docker
./down.sh

docker run --rm --mount source=${DOMAIN}-docker_il-mysql-vol-${DOMAIN},destination=/mysql alpine tar -c -f- mysql | docker run -i --name data-${DOMAIN}-cont alpine tar -x -f-
docker container commit data-${DOMAIN}-cont ${REGISTRY}
docker rm data-${DOMAIN}-cont
# clean log data
rm "${ILIAS_FOLDER}/${DOMAIN}-data/errors/"*
echo "" > "${ILIAS_FOLDER}/${DOMAIN}-data/ilias.log"
docker run --rm -v "${ILIAS_FOLDER}/${DOMAIN}-data/:/extdata/" ${REGISTRY} tar -c -f- extdata | docker run -i --name data-${DOMAIN}-cont ${REGISTRY} tar -x -f-
docker container commit data-${DOMAIN}-cont ${REGISTRY}
docker rm data-${DOMAIN}-cont
docker run --rm -v "${ILIAS_FOLDER}/${DOMAIN}-web/data/:/webdata/" ${REGISTRY} tar -c -f- webdata | docker run -i --name data-${DOMAIN}-cont ${REGISTRY} tar -x -f-
docker container commit data-${DOMAIN}-cont ${REGISTRY}
docker rm data-${DOMAIN}-cont
docker run --rm -v "${ILIAS_FOLDER}/${DOMAIN}/ilias.ini.php:/ilias.ini.php" ${REGISTRY} tar -c -f- ilias.ini.php | docker run -i --name data-${DOMAIN}-cont ${REGISTRY} tar -x -f-
docker container commit data-${DOMAIN}-cont ${REGISTRY}
docker rm data-${DOMAIN}-cont
