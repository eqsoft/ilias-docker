#!/usr/bin/env bash

. ./.env

cd $ILIAS_FOLDER

if [ $# != 3 ]; then
    echo "Please set 'repo branch folder'"
    exit 1
fi

REPO=$1
BRANCH=$2
FOLDER=$3

cd $REPO
git fetch origin
git checkout $BRANCH
git reset --hard origin/$BRANCH

cd ../$FOLDER
git fetch origin
git reset --hard origin/$BRANCH
