; <?php exit; ?>
[server]
http_path = "https://${TPL}.example.com"
absolute_path = "/var/www/html"
presetting = ""
timezone = "Europe/Berlin"

[clients]
path = "data"
inifile = "client.ini.php"
datadir = "/var/www/data"
default = "${TPL}"
list = "0"

[setup]
pass = "$2y$11$JWJc3zjkAOyG33aeikI.8OFTDQzNNXhfqj8S0Q5UwDiQU2Dt.X2MG"

[tools]
convert = "/usr/bin/convert"
zip = "/usr/bin/zip"
unzip = "/usr/bin/unzip"
java = ""
ffmpeg = ""
ghostscript = ""
latex = ""
vscantype = "none"
scancommand = ""
cleancommand = ""
enable_system_styles_management = ""
lessc = ""
phantomjs = "/usr/bin/phantomjs"

[log]
path = "/var/www/data"
file = "ilias.log"
enabled = "1"
level = "WARNING"
error_path = "/var/www/data/errors"

[debian]
data_dir = "/var/opt/ilias"
log = "/var/log/ilias/ilias.log"
convert = "/usr/bin/convert"
zip = "/usr/bin/zip"
unzip = "/usr/bin/unzip"
java = ""
ffmpeg = "/usr/bin/ffmpeg"

[redhat]
data_dir = ""
log = ""
convert = ""
zip = ""
unzip = ""
java = ""

[suse]
data_dir = ""
log = ""
convert = ""
zip = ""
unzip = ""
java = ""

[https]
auto_https_detect_enabled = "0"
auto_https_detect_header_name = ""
auto_https_detect_header_value = ""
