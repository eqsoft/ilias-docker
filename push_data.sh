#!/bin/sh

. ./.env

CWD=$(pwd)

if [ $# != 1 ] ; then
    echo "Usage: $0 [DOMAIN]"
    exit 0
fi

DOMAIN=$1
REGISTRY="registry.gitlab.com/eqsoft/ilias-data:${DOMAIN}"

docker login registry.gitlab.com
docker push ${REGISTRY}
