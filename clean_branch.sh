#!/usr/bin/env bash

. ./.env

CWD=$(pwd)

if [ $# != 1 ] ; then
    echo "Usage: $0 [BRANCH]"
    exit 0
fi

BRANCH=$1

cd ../${BRANCH}-docker
./down.sh

cd ${ILIAS_FOLDER}

if [ -f ./${BRANCH}-data/ilias.log ] ; then
    rm -r ./${BRANCH}-data/*
fi

if [ -d ./${BRANCH}-web/data/${BRANCH} ] ; then
    rm -r ./${BRANCH}-web/data/*
fi

if [ -f ./${BRANCH}-web/ilias.ini.php ] ; then
    rm ./${BRANCH}-web/ilias.ini.php
fi

docker volume rm ${BRANCH}-docker_il-mysql-vol-${BRANCH}
docker volume rm ${BRANCH}-docker_il-lucene-vol-${BRANCH}

cd ${CWD}/../${BRANCH}-docker
./up.sh
