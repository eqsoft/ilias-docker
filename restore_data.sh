#!/bin/sh

. ./.env

CWD=$(pwd)

if [ $# != 1 ] ; then
    echo "Usage: $0 [DOMAIN]"
    exit 0
fi

DOMAIN=$1
REGISTRY="registry.gitlab.com/eqsoft/ilias-data:${DOMAIN}"

# ensure docker container are down
cd ../${DOMAIN}-docker
./down.sh

docker run --rm ${REGISTRY} tar -c -f- mysql | docker run -i --rm --mount source=${DOMAIN}-docker_il-mysql-vol-${DOMAIN},destination=/mysql ${REGISTRY} tar -x -f-

if [ -d "${ILIAS_FOLDER}/${DOMAIN}-web/data/${DOMAIN}" ] ; then
    echo "remove ${ILIAS_FOLDER}/${DOMAIN}-web/data/*"
    rm -r "${ILIAS_FOLDER}/${DOMAIN}-web/data/"*
fi
if [ -f "${ILIAS_FOLDER}/${DOMAIN}-data/ilias.log" ] ; then
    echo "remove ${ILIAS_FOLDER}/${DOMAIN}-data/*"
    rm -r "${ILIAS_FOLDER}/${DOMAIN}-data/"*
fi
if [ -f "${ILIAS_FOLDER}/${DOMAIN}-web/ilias.ini.php" ] ; then
    echo "${ILIAS_FOLDER}/${DOMAIN}-web/ilias.ini.php"
    rm "${ILIAS_FOLDER}/${DOMAIN}-web/ilias.ini.php"
fi

if [ ! -d "${ILIAS_FOLDER}/${DOMAIN}-web/data" ] ; then
    mkdir "${ILIAS_FOLDER}/${DOMAIN}-web/data"
fi

if [ ! -d "${ILIAS_FOLDER}/${DOMAIN}-data" ] ; then
    mkdir "${ILIAS_FOLDER}/${DOMAIN}-data"
fi

docker create -ti --name dummy ${REGISTRY} /bin/sh
docker cp dummy:/webdata/. "/${ILIAS_FOLDER}/${DOMAIN}-web/data"
docker cp dummy:ilias.ini.php "/${ILIAS_FOLDER}/${DOMAIN}-web"
docker cp dummy:/extdata/. "/${ILIAS_FOLDER}/${DOMAIN}-data"
docker rm -f dummy
