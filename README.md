# ilias-docker
A bundle of tools for creating and accessing flexible ilias environments and related systems for testing and development purposes.

# Basic Requirements
  * A Linux environment like Debian 10 or Ubuntu > 18.4 is recommanded
  * git
  * docker and docker-compose (see install docu for your os)
  * modern browser and ssh-client for accessing the internal docker webapplications

# Basic Concept
Testing and development scenarios often requires multiple infrastrucures and application configs for diverse purposes.
This leads to an expensive provision of various tools, installation candidates, third-party software and network infrastructure. 
With the straight use of container based micro services the load can be reduced and also the transparency increases significantly due to a complete technical documentation of the whole infrastructure.

The required microservices can be configured and plugged into a virtual docker network which supports a simple domain based service layer and a valide ssl workflow within a wildcard *.example.com pki.
You can create and plug as many xxx.example.com services as you need, p.e. 
ltiprovider60.example.com (ilias), lticonsumer54.example.com (ilias), lrs.example.com (learninglocker), content.example.com (static xapi courses), mathjax.example.com ...
The ILIAS code can be checked out from any git repo and used within a defined domain context. 

ILIAS code/data and external/data are mounted from the host into docker container. Every ILIAS service gets its own mysql database with a persistent docker-volume layer, but it is also practicable to define a shared database for multiple ilias instances for saving ressources.

For accessing the whole docker infarstructure it is important to understand the underlying concept:

  * All services defined in the docker reciepts can be accessed by their service names as hostnames and additional defined aliases p.e. nginx webserver: DOMAIN.example.com. The docker network is completely closed and there is no port-forwarding / mapping defined. 
  * By starting a special ilias-sshd service within the docker network you can now access the docker web applications through a ssh-tunnel from your hosts browser (see **Accessing the docker network**).
  * The benfits of such a concept are that there are no side-effects from various port-mappings, the DNS requests are resovled directly from within the docker network dns, the behaviour of the whole infrastructure including the browser client is like a real internet based environment, just private and enclosed for special testing or development purposes.
  * Another tremendous advantage is that this can be provided also by a public host over the internet. The same infrastructure can be started and accessed by multiple users over a secure ssh-tunnel.
  * The various docker infrastrutures can be developped and used locally and online by switching the ssh-tunnel endpoint from your local browser (see **Accessing the docker network**).

# Quickstart

--------------------------------------------
It is NOT recommanded to process as root user. Also you should first configure a user-namespace remapping for docker container like described here:

[https://www.jujens.eu/posts/en/2017/Jul/02/docker-userns-remap](https://www.jujens.eu/posts/en/2017/Jul/02/docker-userns-remap/)

Otherwise the root user in your docker container is the real root user on your host!

`/etc/docker/daemon.json` should look like this:

```
{
  "userns-remap": "USER"
}
```
Where "USER" is your username.

`/etc/subuid` should look like this:

```
USER:1000:1
USER:100000:65536
```
Where "USER" is your username and "1000" you can get with `id -u USER`

`/etc/subgid` should look like this:

```
USER:998:1
USER:100000:65536
```
Where "USER" is your username and "998" you can get with `getent group docker`

Restart your docker service:

```
systemctl restart docker
```
-------------------------------------------------
Provide a folder structure on the host like this (check if not exists already):

```
sudo mkdir /opt/ilias
sudo mkdir /opt/docker
sudo chown -R USER:GROUP /opt/ilias
sudo chown -R USER:GROUP /opt/docker
```

Where USER is your username and GROUP your groupname.

The following example uses code branches from the original ILIAS repo. You can use any other ILIAS repos / forks

```
cd /opt/ilias
git clone https://github.com/ILIAS-eLearning/ILIAS.git ilias-orig
cd /opt/docker
git clone https://gitlab.com/eqsoft/ilias-docker.git
```

Adjust .env params to your environment (can be skipped if /opt/docker and /opt/ilias pathes from that docu are used)

---------------------------------------------------------------
Next get the pki:

```
cd /opt/docker
git clone https://gitlab.com/eqsoft/pki.git
```

Next create an ILIAS code instance from ilias-orig/trunk branch with domainname **trunk**.example.com.

```
cd /opt/docker/ilias-docker
./create_branch.sh ilias-orig trunk trunk
```

This will checkout and pull from ilias-orig **repo (1. param)** the current trunk **branch (2. param)** to the **folder /opt/ilias/trunk (3. param)**, create an additional trunk-data folder for external ILIAS data and symlink trunk-web -> trunk

Next create an associated docker environment:

```
cd /opt/docker/ilias-docker
./create_domain.sh trunk
```

This will create a folder /opt/docker/trunk-docker with all required reciepts and scripts to run an ILIAS installation that can be accessed by trunk.example.com. 

Next start the environment:

```
cd /opt/docker/trunk-docker
./up.sh
```

Check the docker container:
```
docker container ls
```

# Accessing the docker network

(yet linux only... the instruction for windows hosts will follow...)
All ILIAS docker container are part of the same network. The `up.sh` script ensures that the network is created once if it still does not exist. The network is persistent and usually you don't need to edit the network unless `172.100.0.0/16` is already in use on your host. You can ajust the subnet range in the `.env` file.

Create a public/privat ssh-key pair or use an existing one. Assign a strong password so you can use it for both local and remote environments.
Clone the ilias-sshd repo to `/opt/docker/ilias-sshd`, add your pubkey to .ssh/autorized_keys and start the container:

```
ssh-keygen -t rsa -b 4096 -f ~/.ssh/docker.key
cd /opt/docker
git clone https://gitlab.com/eqsoft/ilias-sshd.git
cd ilias-sshd && mkdir .ssh
cat ~/.ssh/docker.key.pub > .ssh/authorized_keys
chmod 700 .ssh
chmod 644 .ssh/authorized_keys
./up.sh
```

If you did not follow the instructions for remapping the root docker user, you have to change the rights of ilias-sshd/.ssh folder to the real root user:

```
./down.sh
sudo chown -R root.root .ssh
./up.sh

```

Now there is a ssh daemon running in the docker network and is listening on port 443 for incomming requests.
Next we need to establish a ssh-tunnel and a browser that supports SOCKS5 proxy for tunneling all the requests through the ssh-tunnel into the docker environment.

You can use the `ilias-tunnel` repo with a preconfigured firefox, a tunnel script and ssh-config template:

```
cd ~
git clone https://gitlab.com/eqsoft/ilias-tunnel.git
cd ilias-tunnel
cp tpl.config config
```

Replace `IP_TO_HOST_1` with the ip of an external docker server (if exists) and `PATH_TO_PRIVATE_KEY` in both entries to your ssh private key e.g. `~/.ssh/docker.key`.
Now you can start the ssh-tunnel and preconfigured firefox to your local docker environment:

```
./up.sh local
```

or external docker server:

```
./up.sh vpn
```

Type `https://trunk.example.com` into browser addressbar if you followed the example above.

# Store and restore ILIAS application data

Testing- and development environments often needs predefined application configurations and initial data to play with and after processing tests the application should be reset to the predfined initial state.

After creating an application branch in a domain context like described above the application can be configured and populated with initial data manually. After finishing the desired state can be stored with `./store_data.sh DOMAIN` as ilias-data:DOMAIN container and restored with `restore_data.sh DOMAIN`.

The data is stored online as lightwise alpine docker image in my gitlab registry storage. If you use your own resgistry you have to adjust the registry url in the scripts. If you want to use this registry please contact me for access rights. This also applies to other cooperations.

# Available ILIAS docker services

The basic ILIAS infrastructure is defined in the [tpl.docker-compose.yml](tpl.docker-compose.yml) and [.env](.env).

By creating a domain with `create_domain.sh DOMAIN` the ${TPL} variable is replaced by the given DOMAIN parameter and all template files are stored in the ../DOMAIN folder (e.g. `../trunk/docker-compose.yml` etc.).

By default there is no need to adjust any of the predefined parameters.

The microservices are pre-build and stored in a regulare gitlab registry. There is no need to build the images at your own. If they still not exist locally they are pulled just in time. The following services are started within the basic ILIAS docker domain environment:

  * **ilias-nginx**: [https://gitlab.com/eqsoft/ilias-nginx.git](https://gitlab.com/eqsoft/ilias-nginx.git) / latest image: registry.gitlab.com/eqsoft/ilias-nginx:latest
  * **ilias-php**: [https://gitlab.com/eqsoft/ilias-php.git](https://gitlab.com/eqsoft/ilias-php.git) / latest image: registry.gitlab.com/eqsoft/ilias-php:fpm 
  * **ilias-java**: [https://gitlab.com/eqsoft/ilias-java.git](https://gitlab.com/eqsoft/ilias-java.git) / latest image: registry.gitlab.com/eqsoft/ilias-java:8 (the container is started in a loop but the javasserver needs to be started if required manually after regular ILIAS setup `java_start.sh`)
  * **mariadb 10.2**: from dockerhub: [https://hub.docker.com/_/mariadb](https://hub.docker.com/_/mariadb)

The following services can be started and accessed by all domains:

  * **ilias-mathjax**: [https://gitlab.com/eqsoft/ilias-mathjax.git](https://gitlab.com/eqsoft/ilias-mathjax.git) / latest image: registry.gitlab.com/eqsoft/ilias-mathjax:latest
  * **ilias-pma** (phpMyAdmin): [https://gitlab.com/eqsoft/ilias-pma](https://gitlab.com/eqsoft/ilias-pma) there is no prebuild image, just run with `up.sh`
  * **ilias-learninglocker**: [https://gitlab.com/eqsoft/ilias-learninglocker](https://gitlab.com/eqsoft/ilias-learninglocker): copy `tpl.env` to `.env`, adjust parameter to your environment, run `build-dev.sh` and `up.sh`. Please refer to README.md.

