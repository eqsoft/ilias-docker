#!/usr/bin/env bash

git clone https://gitlab.com/eqsoft/ilias-content.git content
ln -sf content content-web
mkdir content-data
